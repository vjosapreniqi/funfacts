package com.example.botek.funfacts;

import java.util.Random;

public class FactBook {
    // Vargu i fakteve
    public String[] mFacts = {
            "Milingonat zgjerohen kur zgjohen në mëngjes.",
            "Makushet mund të vrapojnë më shpejt se kuajt.",
            "Medalet e arta olimpike në të vertetë së shumëti përbëhen nga argjendi.",
            "Ju keni lindur me 300 eshtra, Pas një kohe, kur të rriteni, do t'i keni vetëm 206 eshtra.",
            "Nevoiten vetëm 8 minuta që drita të arrijë nga Dielli ne Tokë.",
            "Disa dru bambuje mund të rriten nje metër brenda një dite.",
            "Shteti i Floridas është më i madh se Anglia.",
            "Disa pinguinë mund të kërcejnë 2-3 metra jasht ujit.",
            "Mesatarisht, nevoiten 66 ditë për të krijuar një shprehi.",
            "Mamuthët kanë jetuar gjat epokës së akullnajave."};



    public String getFact(){

        String fact = "";
        //Selektimi i nje fakti ne menyre te rastesishme
        Random randomGenerator=new Random();//Konstrukto nje random generator
        int randomNumber = randomGenerator.nextInt(mFacts.length);
        fact = mFacts[randomNumber];
        return fact;



    }
}
